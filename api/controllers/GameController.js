/**
 * GameController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */
const game = require('../services/Game');
module.exports = {

  startGame: async (req,res) => {
    let player = req.param("player");
    let startTime = new Date();
    let id = startTime.getTime();
    let cities = await game.createNewGame(player,startTime);
    return res.json({success:true,cities:cities,startTime:startTime,id:id});
  },

  getAnswer: (req,res) => {
    let player = req.param("player");
    let gameId = req.param("id");
    let cityId = req.param('cityId');
    let guess = req.param('guess');
    let currentGame = Game.getGame(player,gameId);
    if(currentGame) {
      let currentGameStatus = game.getAnswer(currentGame,cityId,guess);
      if (currentGameStatus) {
        return res.json({success:true,result:currentGameStatus});
      }
      else {
        return res.json({success:false,message:"Invalid answer - already guessed it or unknown city id"});
      }
    }
    else {
      return res.json({success:false,message:'Game Over'});
    }
  }
};

