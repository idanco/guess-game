/**
 * Game.js
 *
 * @description :: A model definition represents a game
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

const weatherApi = require('./WeatherApi');

//private functions
getRandomCities = (arr,n) => {
  let result = new Array(n),
    len = arr.length,
    taken = new Array(len);
  if (n > len)
    throw new RangeError("getRandom: more elements taken than available");
  while (n--) {
    let x = Math.floor(Math.random() * len);
    result[n] = arr[x in taken ? taken[x] : x];
    taken[x] = --len in taken ? taken[len] : len;
  }
  return result;
};
module.exports = {
  list: {},
  STATUS: {"NEW":"NEW","FINISHED":"FINISHED"},
  RESULT: {"SUCCESS":"SUCCESS","FAILED":"FAILED"},
  cities: {
    833: {
      "id": 833,
      "name": "Ḩeşār-e Sefīd",
      "state": "",
      "country": "IR",
      "coord": {
        "lon": 47.159401,
        "lat": 34.330502
      }
    },
    2960: {
      "id": 2960,
      "name": "‘Ayn Ḩalāqīm",
      "state": "",
      "country": "SY",
      "coord": {
        "lon": 36.321911,
        "lat": 34.940079
      }
    },
    3245: {
      "id": 3245,
      "name": "Taglag",
      "state": "",
      "country": "IR",
      "coord": {
        "lon": 44.98333,
        "lat": 38.450001
      }
    },
    3530: {
      "id": 3530,
      "name": "Qabāghlū",
      "state": "",
      "country": "IR",
      "coord": {
        "lon": 46.168499,
        "lat": 36.173302
      }
    },
    294800: {
      "id": 294800,
      "name": "Haifa",
      "state": "",
      "country": "IL",
      "coord": {
        "lon": 35.0,
        "lat": 32.583328
      }
    },
    5174: {
      "id": 5174,
      "name": "‘Arīqah",
      "state": "",
      "country": "SY",
      "coord": {
        "lon": 36.48336,
        "lat": 32.889809
      }
    }
  },

  configuration: {
    errorRange: 10,
    numberOfCities: 5,
    gameTimeInMinutes: 5
  },

  // Main Functions


  buildCities: async () => {
    let cities = getRandomCities(Object.values(JSON.parse(JSON.stringify(Game.cities))),Game.configuration.numberOfCities);
    let cityGuessResult = {};
    for (let i=0; i<cities.length; i++) {
      let city = cities[i];
      city.weather = await weatherApi.getWeatherByCity(city.id);
      cityGuessResult[city.id] = {weather:city.weather,guess:null};
    }
    return {cities:cities,cityGuessResult:cityGuessResult};
  },
  createNewGame: async (player,startTime) => {
    let citiesObject = await Game.buildCities();
    let cities = citiesObject.cities;
    let cityGuessResult = citiesObject.cityGuessResult;
    let newGame = {cities:cities,cityGuessResult:cityGuessResult,player:player,startTime:startTime,endTime:null,status:Game.STATUS.NEW,result:null,rightGuess:0,wrongGuess:0,check:cities.length};
    Game.list[startTime.getTime()+"_"+player] = newGame; // the key is timestamp + unique player name
    return newGame.cities;
  },
  getGame: (player,gameId) => {
    let game = Game.list[gameId+"_"+player];
    if (game && game.status===Game.STATUS.NEW) {
      let now = new Date();
      let diff = now - game.startTime;
      let diffMins = Math.round(((diff % 86400000) % 3600000) / 60000);
      if (diffMins > Game.configuration.gameTimeInMinutes) {
        game.status = Game.STATUS.FINISHED;
        game.result = Game.RESULT.FAILED;
        return false; // time over
      }
      else {
        return game;
      }
    }
    else {
      return false; // no game
    }
  },

  getAnswer: (currentGame,cityId,guess) => {
    if (currentGame.cityGuessResult[cityId] && !currentGame.cityGuessResult[cityId].guess) {
      currentGame.cityGuessResult[cityId].guess = guess;
      if (Math.abs(guess-currentGame.cityGuessResult[cityId].weather) <= Game.configuration.errorRange) {
        currentGame.rightGuess++;
      }
      else {
        currentGame.wrongGuess++;
      }
      currentGame.check--;
      if (!currentGame.check) {
        currentGame.status = Game.STATUS.FINISHED;
        currentGame.result = currentGame.wrongGuess === 0 ? Game.RESULT.SUCCESS : Game.RESULT.FAILED;
      }
      return currentGame;
    }
    else {
      return false;
    }
  }
};

