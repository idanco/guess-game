/**
 * WeatherApi.js
 *
 * @description :: A service api definition for weather in cities.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */
const request = require('request');
module.exports = {

  attributes: {
    baseUrl: 'https://api.openweathermap.org',
    appId: 'dba163026ff4b0c82f5554f8c314b615',
    action: '/data/2.5/weather',
    units: 'metric'
  },

  /**
   * get city id and return the weather right now
   */
  getWeatherByCity: (cityId) => {
    return new Promise(function (resolve, reject) {
      request(WeatherApi.attributes.baseUrl + WeatherApi.attributes.action + "?id=" + cityId + "&units=" + WeatherApi.attributes.units + "&appid=" + WeatherApi.attributes.appId, function (error, response, body) {
        if (!error && response.statusCode === 200) {
          try {
            let result = JSON.parse(body);
            if (result.main && result.main.temp) {
              resolve(result.main.temp);
            }
            else {
              reject("missing information in result");
            }
          }
          catch(ex) {
            reject(ex.message);
          }
        } else {
          reject(error);
        }
      });
    });
  }

};

